#include <stdlib.h>
#include <stdio.h>
#include "a2support.h"

#define SIZE 5
#define TEST_SUCCESS 1
#define TEST_FAILURE 0

/* Test program used to test each module created */
int testInts( int * );
int testFloats( float * );

int main( void )
{
	int numbers[5] = { 1, 2, 3, 4, 5 };
	float floats[5] = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };


	printf( "Testing integers...\n" );

	if( testInts( numbers ) )
		printf( "Integer test successful!\n" );
	else printf( "Integer test failed!\n");


	// Test to make sure it can handle a null pointer
	if( testInts( NULL ) )
		printf( "Failed Integer test, CONGRATS!\n" );


	printf( "Testing floats...\n" );

	if( testFloats( floats ) )
		printf( "Float test successful!\n" );
	else printf( "Float test failed!\n" );


	// Test countbits()
	printf( "Testing countBits( int ): \n" );

	printf( "The number 2 has %d bits.\n", countBits( 2 ) );

	printf( "The number 6 has %d bits.\n", countBits( 6 ) );
	printf( "The number -1 has %d bits.\n", countBits( -1 ) );
	printf( "The number b has %d bits.\n", countBits( 'b' ) );

	printf( "The number 2 actually has 1 bit.\n" );
	printf( "The number 6 actually has 2 bits.\n" );

	printf( "The number -1 actually has 1 bit.\n" );

	printf( "Testing binaryString(): \n" );

	char * str1 = NULL;
	char * str2 = NULL;
	binaryString( &str1, 6, 2 );
	binaryString( &str2, 8, 6 );
	printf( "The binary represenation of 2 is %s\n", str1 );
	printf( "The binary represenation of 6 is %s\n", str2 );
	printf( "Finish testing, terminating.\n" );

	return EXIT_SUCCESS;
}

int testInts( int * iArray )
{
	if( !iArray )
		return TEST_FAILURE;

	showIntegers( iArray, SIZE );
	printf( "The median of iArray is: %d\n", medianInteger( iArray, SIZE ) );

	return TEST_SUCCESS;
}

int testFloats( float * fArray )
{
	if( !fArray )
		return TEST_FAILURE;

	showFloats( fArray, SIZE );
	printf( "The median of fArray is: %f\n", medianFloat( fArray, SIZE ) );

	return TEST_SUCCESS;
}
