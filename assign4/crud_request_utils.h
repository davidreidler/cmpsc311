//
// CRUD Request/Response Utilities

// Creates an initialization request (which is just the number of 0 so no args)
CrudRequest crud_create_request_init( void );

// Creates a CRUD_CREATE request with the specified length
CrudRequest crud_create_request_create( uint32_t len, CRUD_FLAG_TYPES );

// Creates a CRUD_READ request with the specified Object ID
CrudRequest crud_create_request_read( CrudOID oid, CRUD_FLAG_TYPES );

// Creates a CRUD_UPDATE request with the specified Object ID and length
CrudRequest crud_create_request_update( CrudOID oid, uint32_t len, CRUD_FLAG_TYPES );

// Creates a CRUD_DELETE request with the specified Object ID
CrudRequest crud_create_request_delete( CrudOID oid, CRUD_FLAG_TYPES );

// Creates a CRUD_FORMAT request 
CrudRequest crud_create_request_format( void );

// Creates a CRUD_CLOSE request
CrudRequest crud_create_request_close( void );

// Determines if a request was completed successfuly
uint64_t crud_operation_success( CrudResponse res );

// Determines the Object ID from a CrudResponse
CrudOID crud_extract_oid( CrudResponse res );

// Determines the size of the object from a CrudResponse
uint32_t crud_extract_size( CrudResponse res );

