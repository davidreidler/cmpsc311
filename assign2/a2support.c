////////////////////////////////////////////////////////////////////////////////
//
//  File          : a2support.c
//  Description   : This file contains the implementation for all support 
//                  functions listed in a2support.h. These are used in main to
//                  preform all necessary operations specified in the handout.
//
//  Author        : David Reidler
//  Date Created  : September 12, 2014	

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include "a2support.h"

////////////////////////////////////////////////////////////////////////////////
// 
//  Function     : showFloats
//  Description  : This function takes an array of floats and prints them to the
//                 screen.
//
//  Inputs       : Array of floats, length of the array
//  Returns      : Nothing
void showFloats( float * fArray, int len )
{
	int i;

	if( !fArray )
		return;

	// Print all of the integers in the array
	for( i = 0; i < len; i++ )
	{
		printf( "%*.*f", 7, 2, fArray[i] );
	}

	// Print out the extra newline each time
	printf( "\n" );

	return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Functions    : showIntegers
//  Description  : This function takes an array of integers and prints them to
//                 the screen.
//
//  Inputs       : Array of integers, length of the array
//  Returns      : Nothing
void showIntegers( int * iArray, int len )
{
	int i;

	if( !iArray )
		return;

	// Print all the integers in the array to the screen
	for( i = 0; i < len; i++ )
	{
		printf( "%*d", 7, iArray[i] );
	}

	// Print out the extra newline each time
	printf( "\n" );

	return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : medianFloat
//  Description  : Given a sorted floating array, this function will find the
//                 median value of the array.
//
//  Inputs       : Array of floats, length of the array.
//  Returns      : A float containing the median value of the array.
float medianFloat( float * fArray, int len )
{
	// If the array is sorted then we need to determine if it has an odd
	// or even number of values

	// if len is odd 
	if( len % 2 == 1 )
	{
		return fArray[( len/2 ) - 1];
	} else { // len is even 
		return ( ( fArray[( len/2 ) - 1] ) + ( fArray[( len/2 )]) ) / 2.0;	
	}
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : medianInteger
//  Description  : Given a sorted integer array, this function will find the 
//                 median value of the array.
//
//  Inputs       : Array of integers, length of the array
//  Returns      : A float containing the median value of the array
float medianInteger( int * iArray, int len )
{
	// len is odd
	if( len % 2 == 1 )
	{
		return ( float ) iArray[( len/2 ) - 1];
	} else { // len is even
		return ( (iArray[( len/2 ) - 1]) + iArray[( len/2 )] ) / 2.0;
	}
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : countBits
//  Description  : This function takes an integer and computes the number of 
//                 set bits, or '1' bits in the number.
//
//  Inputs       : A number whos bits we are going to count.
//  Returns      : The number of set bits the number has.
int countBits( int number )
{
	int counter = 0;
	int i;

	// We are going to ignore the sign bit which is the last bit
	for( i = 0; i < ( sizeof( number ) * 8) - 1; i++ )
	{
		// Increase 1 if bit is 1, or increase 0 if bit is zero
		// Shift all bits to the right for the next interation
		counter = counter + ( number & 1 );
		number = number >> 1;
	}

	return counter;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : reverseBits
//  Description  : This function takes a number and reverses the bit values 
//                 of the number.
//
//  Inputs       : A number whos bits will be reversed
//  Returns      : A number containing the reversed bitset of the original 
//                 number.
unsigned short reverseBits( unsigned short number )
{
	unsigned short result = 0;
	int i;
	for( i = 0; i < ( sizeof( number ) * 8 ); i++ )
	{
		// Shift everything in the result left a bit
		// then appened the correct bit by adding
		// finally shift the number right to get the next bit
		result = result << 1;
		result = result + ( number & 1 );
		number = number >> 1;
	} 

	return result;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : binaryString
//  Description  : Creates a string which is the binary representation of the
//                 specified number with the specified length.
//
//  Intputs      : A pointer to where the string should be created, the length
//                 of the string, the number which will be represented by the
//                 string.
//  Returns      : None
void binaryString( char ** buffer, int len, unsigned short number )
{
	// Avoid memory leaks
	if( *buffer ) free( *buffer );

	// Remember to account for NULL character endings 
	( *buffer ) = ( char * ) malloc( len + 1 );
	
	int i;
	for( i = len - 1; i >= 0; i-- )
	{
		if( number & 1 ) ( *buffer )[i] = '1';
		else ( *buffer )[i] = '0';

		number = number >> 1;
	}

	// Make sure that we write the NULL character at the very end
	( *buffer )[len] = '\0';
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : floatQuickSort
//  Description  : A recursive function which will sort a given float array
//                 using the quick-sort algorithm from highest to lowest.
//
//  Inputs       : An array of floats, the index to start sorting at and the 
//                 the index to stop sorting at.
//  Returns      : None
void floatQuickSort( float * fArray, int lIndex, int rIndex )
{
	if( !fArray ) return;

	if( lIndex < rIndex )
	{
		int q = partition_HL( fArray, lIndex, rIndex );
		floatQuickSort( fArray, lIndex, q - 1);
		floatQuickSort( fArray, q + 1, rIndex );
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : integerQuickSort
//  Description  : A recursive function which will sort a specified integer
//                 array using quick-sort from lowest to highest.
//
//  Inputs       : An array of integers, the index to start sorting at, and the
//                 index to stop sorting at.
//  Returns      : None
void integerQuickSort( int * iArray, int lIndex, int rIndex )
{
	if( !iArray ) return;

	if( lIndex < rIndex )
	{
		int q = partition_LH( iArray, lIndex, rIndex );
		integerQuickSort( iArray, lIndex, q - 1 );
		integerQuickSort( iArray, q + 1, rIndex );
	}

	return;
} 

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : partition_HL
//  Description  : Partitions and reorders a float array within specified
//                 indicies of the array.
//
//  Inputs       : An array of floats, the index to start partitioning at, and 
//                 the index to stop partitioning at
//  Returns      : The index to base the next partition off of.
int partition_HL( float * fArray, int lIndex, int rIndex )
{
	float pivot = fArray[rIndex];

	int i = lIndex - 1;
	int j;
	for( j = lIndex; j < rIndex; j++ )
	{
		// This comparison determines high-lo or lo-high sorting
		if( fArray[j] > pivot )
		{
			i = i + 1;

			// Swap A[j] and A[i]
			float tmp = fArray[i];
			fArray[i] = fArray[j];
			fArray[j] = tmp;
		}
	}

	float tmp = fArray[i + 1];
	fArray[i + 1] = fArray[rIndex];
	fArray[rIndex] = tmp;

	return i + 1;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : partition_LH
//  Description  : Partitions and reorders an integer array within specified
//                 indicies of the array.
//
//  Inputs       : An array of integers, the index to start paritioning at, and
//                 the index to stop partitioning at.
//  Return       : The index to base the next parition off of.
int partition_LH( int * iArray, int lIndex, int rIndex )
{
	// Temp storage for swapping A[j] and A[i]
	int pivot = iArray[rIndex];

	int i = lIndex - 1;
	int j;
	for( j = lIndex; j < rIndex; j++ )
	{
		// This comparison determines high-low or low-high sorting
		if( iArray[j] < pivot )
		{
			i = i + 1;

			// Swap A[j] and A[i]
			int tmp = iArray[i];
			iArray[i] = iArray[j];
			iArray[j] = tmp;
		}
	}

	int tmp = iArray[i + 1];
	iArray[i + 1] = iArray[rIndex];
	iArray[rIndex] = tmp;

	return i + 1;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : showCDF
//  Description  : This function takes an integer array and shows the CDF graph
//                 which is the cumulative distribution graph, where the y
//                 axis is the probability from 100% to 0% stepping down by 5%
//                 and the x axis shows the range of values from low to high.
//
//  Inputs       : An integer array and the length of the array.
//  Returns      : None
void showCDF( int * iArray, int len )
{
	int distance = iArray[len - 1] - iArray[0] + 1;
	float probabilityArray[distance];
	int i, j, pi, numStars, counter;

	// 21 lines because 21 percentiles from 0% - 100%
	char ** lines = malloc( sizeof( char * ) * 21 );

	// Number of characters = distance, plus 1 for null character
	for( i = 0; i < 21; i++ )
		lines[i] = malloc( distance + 1 );

	// Step 1: Build probabilities
	// From the last to the beginning
	for( i = iArray[len - 1], pi = distance - 1; i >= iArray[0]; i--, pi-- )
	{
		counter = 0;
		// Start from the beginning to the end
		for( j = 0; j < len; j++ )
		{
			if( iArray[j] <= i )
				counter++;
		}

		probabilityArray[pi] = ( 100 * counter )/len;
	}

	// Step 2: From the last string up, create strings based on the
	//         Probability Distribution.
	for( i = 20; i >= 0; i-- )
	{
		numStars = 0;
		for( pi = 0; pi < distance; pi++ )
		{
			if( probabilityArray[pi] >= (float)( i*5 ) )
				numStars++;
		}
		
		for( j = 0; j < distance - numStars; j++ )
		{
			lines[i][j] = ' ';
		}

		for( j = distance - numStars; j < distance; j++ )
			lines[i][j] = '*';

		// Don't forget about null termination
		lines[i][distance] = '\0';
	}

	// Step 3: Print each line with printf
	for( i = 20; i >= 0; i-- )
		printf( "%3d +%s\n", i*5, lines[i] );

	// Step 4: Print the X-axis
	printXAxis( iArray, len );
	return;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : printXAxis
//  Description  : Prints the xaxis on the screen
//
//  Inputs       : The array of integers and the length of the array.
//  Returns      : None
void printXAxis( int * iArray, int len )
{
	int distance = iArray[len - 1] - iArray[0] + 1;
	int highest = iArray[len - 1];
	int numDigits = 1;
	int i, j;                                           // Iterators
	while( highest >= 10 )
	{
		numDigits++;
		highest = highest / 10;
	}

	// Prints the dashes
	printf( "    +" );
	for( i = 0; i < distance; i++) printf( "-" );
	printf( "\n");

	// Print the n digit
	for( i = numDigits; i > 0; i-- )
	{
		printf( "     " );
		for( j = iArray[0]; j <= iArray[len - 1]; j++ )
		{
			if( (j / power( 10, i - 1 ) ) == 0 ) printf(" ");
			else printf( "%d", ( j / power( 10, i - 1 ) % 10 ) );
		}
		printf( "\n" );
	}
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function     : exp
//  Description  : Takes a number and raises it to a specified power
//
//  Inputs       : The base number and the exponent
//  Returns      : The base number raised to an exponent
int power( int number, int exp )
{
	// We don't care about negative numbers for now
	if( exp == 0 ) return 1;
	else return number * power( number, exp - 1 );
}
