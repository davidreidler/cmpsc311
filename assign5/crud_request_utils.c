////////////////////////////////////////////////////////////////////////////////
//
//  File          : crud_request_utils.c
//  Description   : This is the implementation for creating opcodes used by the
//                  CRUD device.
//
//  Author        : David Reidler
//  Last Modified : Nov 17th, 2014
//
#include "crud_request_utils.h"

//
// CRUD Request/Response Utilities

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_create_request_init
//  Description : Creates an initialization request to send to the object store
//
//  Arguments   : None - A request of 0 is an initialization request
//  Returns     : A CrudRequest (uint64) which has been modified to meet the 
//                request structure.
CrudRequest crud_create_request_init()
{
	// By the request scheme this is an initialization request
	CrudRequest ret = 0;
	return ret;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_create_request_create
//  Description : Creates a CRUD request to create an object whose length is
//                is defined in the request.
//
//  Arguments   : Length of the buffer that will be used to in the request.
//                Since this is a create request, we don't need an OID.
//  Returns     : A CrudRequest representing a create command that can be sent
//                to the object store.
CrudRequest crud_create_request_create( uint32_t len, CRUD_FLAG_TYPES flags )
{
	CrudRequest res = 0;

	// Shift the CRUD_CREATE enum over by 28 bits
	// cast it to make sure it has 64 bits
	res = ( (uint64_t)CRUD_CREATE << 28 );

	// The & operation zero-outs the 4 most significant bits (just in-case)
	res = res + ( ( len & CRUD_MAX_OBJECT_SIZE ) << 4 );

	// Keep the original bitset and add the flag bitset into the request
	res = res + ( flags << 1 );
	return res;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_create_request_read
//  Description : This function creates a read request to send to the object
//                store by structuring it in accordance with the object request
//                protocol.
//
//  Arugments   : The CRUD Object ID that is to be read
//  Returns     : A CrudRequest representing a read command that can be sent to
//                the object store.
CrudRequest crud_create_request_read( CrudOID oid, CRUD_FLAG_TYPES flags )
{
	// CrudOID is 32 bits long, we need at least  64 for the shift 
	CrudRequest res = (uint64_t) oid << 32 ;

	// Insert the read command
	res = res + ( (uint64_t)CRUD_READ << 28 );

	// For reads, we won't know how long the file is exactly, so we should
	// use the max object size
	res = res + ( (uint64_t)CRUD_MAX_OBJECT_SIZE << 4 );

	// Insert the flag bitset
	res = res + ( flags << 1 );

	return res;
}

////////////////////////////////////////////////////////////////////////////////
//  
//  Function    : crud_create_request_update
//  Description : Creates a CRUD request to update a specific object based on
//                the Object's ID.
//
//  Arguments   : The object's ID and the the length of the object.
//  Returns     : A CrudRequest that represents an update command that can be 
//                sent to the object store 
CrudRequest crud_create_request_update( CrudOID oid, uint32_t len, CRUD_FLAG_TYPES flags )
{
	// CrudOID is 32 bits long, we need at least 64 for the shift
	CrudRequest res = (uint64_t)oid << 32 ;

	// Insert the update command
	res = res + ( (uint64_t)CRUD_UPDATE << 28 );

	// Insert the size
	// The & zeros-out the 8 most significant bits of len (just incase)
	res = res + ( ( len & CRUD_MAX_OBJECT_SIZE ) << 4 );

	// Insert the flags
	res = res + ( flags << 1 );

	return res;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_create_request_delete
//  Description : Creates a CrudRequest that represents a delete command on the
//                specified object ID.
//
//  Arguments   : The object ID of the specified object.
//  Returns     : A CrudRequest that represents a delete command that can be
//                sent to the object store.
CrudRequest crud_create_request_delete( CrudOID oid, CRUD_FLAG_TYPES flags )
{
	// CrudIOD is 32 bits long, we need at least 64 for the shift
	CrudRequest res = (uint64_t)oid << 32;

	// Insert the delete command
	res = res + ( (uint64_t)CRUD_DELETE << 28 );

	// Insert the flags
	res = res + ( flags << 1 );

	return res;
}

//
// *** START NEW ASSIGNMENT 4 CODE ***

////////////////////////////////////////////////////////////////////////////////
//
// Function    : crud_create_request_format
// Description : Creates a CrudRequest resembling a CRUD_FORMAT command. No
//               flags are needed because the CRUD device does not use them for
//               format commands.
//
// Input       : N/A
// Returns     : Correctly structured CrudRequest representing a CRUD_FORMAT
//               command.
CrudRequest crud_create_request_format( void )
{
	// We just need to shift 
	CrudRequest res = (uint64_t)CRUD_FORMAT << 28;
	return res;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function    : crud_create_request_close
// Description : This function creates a CrudRequest that resembles a CRUD_CLOSE
//               command for the CRUD device.
//
// Input       : N/A
// Returns     : A CrudRequest which represents a CRUD_CLOSE command and can be
//               sent to the CRUD device.
CrudRequest crud_create_request_close( void )
{
	CrudRequest req = (uint64_t)CRUD_CLOSE << 28;
	return req;
}

// *** END NEW ASSIGNMENT 4 CODE ***
////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_operation_success
//  Description : Determines if a CRUD operation was successful based on the
//                response code that was received.
//
//  Arguments   : A CRUD response code (hopefully) received by the object store
//  Returns     : 0 if successful, 1 if not successful.
uint64_t crud_operation_success( CrudResponse res )
{
	if( res & 1 ) return CRUD_FAILURE; 
	else return CRUD_SUCCESS; 
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_extract_oid
//  Description : Takes a CrudResponse and extracts the Object ID from it.
//
//  Arguments   : The CRUD response from the object store.
//  Returns     : The Object ID in the response.
//
CrudOID  crud_extract_oid( CrudResponse res )
{
	// Result variable
	CrudOID result;

	// Set the result to the most significant 32 bits
	// Since the response is 64 bits, we need to shift 32 bits right
	result = (CrudOID)( res >> 32 );

	return result;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_extract_size
// Description  : This function returns the size of an object from a 
//                CrudResponse.
//
// Inputs       : res - the CrudResponse
// Outputs      : The size of the object in the CrudResponse.
uint32_t crud_extract_size( CrudResponse res )
{
	uint32_t result;

	// Shift right and make sure it has the right bits set
	result = (res >> 4) & (CRUD_MAX_OBJECT_SIZE);
	return result;
}

////////////////////////////////////////////////////////////////////////////////
//
CRUD_REQUEST_TYPES crud_extract_op( CrudResponse res )
{
	return (CRUD_REQUEST_TYPES) ( res >> 28 ) & 15;
}
