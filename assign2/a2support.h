#ifndef A2SUPPORT_INCLUDED
#define A2SUPPORT_INCLUDED

////////////////////////////////////////////////////////////////////////////////
//
//  File          : a2support.h
//  Description   : This is the header file for the functions for assignment
//                  2 of the CMPSC311 course.  Students are required to define
//                  the following functions and implement them in another
//                  file, a2support.c.
//
//  Author        : David Reidler & Professor McDaniel.
//  Created       : September 12, 2014

// Functions to define

void showFloats( float * fArray, int len );

void showIntegers( int * iArray, int len );

float medianFloat( float * fArray, int len );

float medianInteger( int * iArray, int len );

int countBits( int num );

void binaryString( char ** string, int len, unsigned short num );

unsigned short 	reverseBits( unsigned short num );

void floatQuickSort( float * fArray, int lIndex, int rIndex );

void integerQuickSort( int * iArray, int lIndex, int rIndex );

void showCDF( int * iArray, int length );

// START: Custom functions
int partition_HL( float * fArray, int lIndex, int rIndex );
int partition_LH( int * iArray, int lIndex, int rIndex );
int power(int root, int exp );
void printXAxis( int * iArray, int len );
#endif
