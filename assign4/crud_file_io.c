////////////////////////////////////////////////////////////////////////////////
//
//  File           : crud_file_io.h
//  Description    : This is the implementation of the standardized IO functions
//                   for used to access the CRUD storage system.
//
//  Author         : Patrick McDaniel
//  Last Modified  : Mon Oct 20 12:38:05 PDT 2014
//

// Includes
#include <malloc.h>
#include <string.h>

// Project Includes
#include <crud_file_io.h>
#include <cmpsc311_log.h>
#include <cmpsc311_util.h>


// Defines
#define CIO_UNIT_TEST_MAX_WRITE_SIZE 1024
#define CRUD_IO_UNIT_TEST_ITERATIONS 10240

#define CRUD_SUCCESS 0
#define CRUD_FAILURE 1

// Other definitions

// Type for UNIT test interface
typedef enum {
	CIO_UNIT_TEST_READ   = 0,
	CIO_UNIT_TEST_WRITE  = 1,
	CIO_UNIT_TEST_APPEND = 2,
	CIO_UNIT_TEST_SEEK   = 3,
} CRUD_UNIT_TEST_TYPE;

// File system Static Data
// This the definition of the file table
CrudFileAllocationType crud_file_table[CRUD_MAX_TOTAL_FILES]; // The file handle table

// Pick up these definitions from the unit test of the crud driver
CrudRequest construct_crud_request(CrudOID oid, CRUD_REQUEST_TYPES req,
		uint32_t length, uint8_t flags, uint8_t res);
int deconstruct_crud_request(CrudRequest request, CrudOID *oid,
		CRUD_REQUEST_TYPES *req, uint32_t *length, uint8_t *flags,
		uint8_t *res);

//
// Globals
int initialized = 0;
int mounted = 0;

//
// Implementation

// Response/Request Utilities
#include <crud_request_utils.c>

// *** START ASSIGNMENT 4 CODE ***

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_format
// Description  : This function formats the crud drive, and adds the file
//                allocation table.
//
// Inputs       : none
// Outputs      : 0 if successful, -1 if failure

uint16_t crud_format(void) 
{
	// Response from the CRUD bus
	CrudResponse res = 0;

	// Initalize the device if not already done
	if( !initialized )
	{
		CrudResponse res = crud_bus_request( crud_create_request_init(), NULL );
		if( crud_operation_success( res ) == CRUD_FAILURE )
		{
			logMessage( LOG_ERROR_LEVEL, "Failed to initialize CRUD device in format()!" );
			return (-1);
		}
		initialized = 1;
	}

	// Format the device
	res = crud_bus_request( crud_create_request_format(), NULL );
	if( crud_operation_success( res ) == CRUD_FAILURE )
	{
		logMessage( LOG_ERROR_LEVEL, "Failed to format the CRUD device in format()!" );
		return (-1);
	}

	// Zero-out the file allocation table
	int i;
	for( i = 0; i < CRUD_MAX_TOTAL_FILES; i++ )
	{
		memset( &crud_file_table[i].filename[0], 0, sizeof( crud_file_table[i].filename ) );
		crud_file_table[i].object_id = 0;
		crud_file_table[i].position  = 0;
		crud_file_table[i].length    = 0;
		crud_file_table[i].open      = 0;
	}

	// Save the table to the priority object
	res = crud_bus_request( crud_create_request_create( sizeof(crud_file_table), CRUD_PRIORITY_OBJECT ), crud_file_table );
	if( crud_operation_success( res ) == CRUD_FAILURE )
	{
		logMessage( LOG_ERROR_LEVEL, "FAILED to update the priority object in format!" );
		return (-1);
	}

	// Log and return
	logMessage(LOG_INFO_LEVEL, "Format complete!");
	return(0);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_mount
// Description  : This function mount the current crud file system and loads
//                the file allocation table.
//
// Inputs       : none
// Outputs      : 0 if successful, -1 if failure

uint16_t crud_mount(void) 
{
	CrudRequest res;

	// Make sure the device is not already mounted
	if( mounted )
	{
		logMessage( LOG_ERROR_LEVEL, "The device is already mounted!" );
		return (-1);
	}

	// Initialize the system if necessary
	if( !initialized )
	{
		res = crud_bus_request( crud_create_request_init(), NULL );
		if( crud_operation_success( res ) == CRUD_FAILURE )
		{
			logMessage( LOG_ERROR_LEVEL, "You couldn't init the device while mounting!" );
			return (-1);
		}
		initialized = 1;
	}

	// Try and read the priority object
	res = crud_bus_request( crud_create_request_read( 0, CRUD_PRIORITY_OBJECT ), &crud_file_table );
	if( crud_operation_success( res ) == CRUD_FAILURE )
	{
		logMessage( LOG_ERROR_LEVEL, "That's not how you mount a file system!" );
		return (-1);
	}

	// Log, set mounted, return successfully
	logMessage(LOG_INFO_LEVEL, "We actually mounted!" );
	mounted = 1;
	return(0);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_unmount
// Description  : This function unmounts the current crud file system and
//                saves the file allocation table.
//
// Inputs       : none
// Outputs      : 0 if successful, -1 if failure

uint16_t crud_unmount(void) 
{
	CrudResponse res;

	// Make sure the device is mounted and the object store is initialized
	if( !initialized || !mounted )
	{
		logMessage( LOG_ERROR_LEVEL, "You can't unmount something that's not online!" );
		return (-1);
	}

	// Close each file
	int i;
	for( i = 0; i < CRUD_MAX_TOTAL_FILES; i++ )
	{
		crud_file_table[i].open = 0;
	}

	// Write the table to the priority object
	res = crud_bus_request( crud_create_request_update( 0, sizeof(crud_file_table), CRUD_PRIORITY_OBJECT), &crud_file_table );
	if( crud_operation_success( res ) == CRUD_FAILURE )
	{
		logMessage( LOG_ERROR_LEVEL, "Couldn't update the priority object!" );
		return (-1);
	}

	// Issue a CRUD_CLOSE command
	res = crud_bus_request( crud_create_request_close(), NULL );
	if( crud_operation_success( res ) == CRUD_FAILURE )
	{
		logMessage( LOG_ERROR_LEVEL, "We couldn't issue the close command!" );
		return (-1);
	}

	// Log, reset initalized and mounted, return successfully
	initialized = 0;
	mounted = 0;
	return (0);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function    : crud_find_table_entry
// Description : This function takes a path name and finds the file descriptor
//                associated with the filename specified.
//
// Inputs      : The filename to find
// Returns     : The file descriptor of the file in the table
int crud_find_table_entry( char * path )
{
	int i;
	for( i = 0; i < CRUD_MAX_TOTAL_FILES; i++ )
	{
		// Return the index with either the same pathname, or an empty file
		if( strncmp( crud_file_table[i].filename, path, CRUD_MAX_PATH_LENGTH ) == 0 || crud_file_table[i].object_id == 0 )
		{
			return i;
		}
	}

	// There isn't any space left in the table
	return (-1);
}

////////////////////////////////////////////////////////////////////////////////
// 
// Function    : print_table_entry
// Description : This function takes a file descriptor and prints out the table
//               information for that entry. (Mostly used for debugging)
//
// Inputs      : File Descriptor of the file
// Returns     : N/A
void print_table_entry(int16_t fd)
{
	CrudFileAllocationType * entry = &crud_file_table[fd];
	logMessage( LOG_INFO_LEVEL, "FN: %s FD: %d OID: %d  POS: %d  LEN: %d  OPEN: %d",
		entry->filename, fd, entry->object_id, entry->position, entry->length, entry->open );
	return;
}

// *** END ASSSIGNMENT 4 CODE ***

// *** START ASSIGNMENT 3 CODE ***

//
// CRUD File I/O

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_open
// Description  : This function opens the file and returns a file handle
//
// Inputs       : path - the path "in the storage array"
// Outputs      : file handle if successful, -1 if failure

int16_t crud_open(char *path) 
{

	// The response returned from the crud_bus
	CrudResponse res;

	// Entry based on the path - (-1) if the table is full, the fh of the file, or a new fh.
	int16_t entry;

	// Make sure the filesystem is online
	if( !mounted )
	{
		logMessage( LOG_ERROR_LEVEL, "The device is not mounted!" );
		return (-1);
	}

	// Make sure that there's an acceptable path specified!
	if( path == NULL || strlen( path ) > CRUD_MAX_PATH_LENGTH )
	{
		logMessage( LOG_ERROR_LEVEL, "Path is NULL or larger than the max length in open()!\n" );
		return (-1);
	}

	// Find the fh for the filename, or a new entry for a new file.
	entry = crud_find_table_entry( path );
	if( entry == -1)
	{
		// We couldn't find the file
		logMessage( LOG_ERROR_LEVEL, "Could not create a new file in open()! (Table is full)" );
		return (-1);
	}

	// We can't open an already opened file
	if( crud_file_table[entry].open == 1 )
	{
		logMessage( LOG_ERROR_LEVEL, "OPEN bit is trying to be set on an open file in open!" );
		return (-1);
	}

	// This is a new file
	if( crud_file_table[entry].object_id == 0 )
	{
		// Make a write to the bus
		res = crud_bus_request( crud_create_request_create( 0, CRUD_NULL_FLAG ), NULL );
		if( crud_operation_success( res ) == CRUD_FAILURE )
		{
			logMessage( LOG_ERROR_LEVEL, "Failed to create the object in open()!" );
			return (-1);
		}
	
		// Copy the important parts
		strncpy( crud_file_table[entry].filename, path, CRUD_MAX_PATH_LENGTH );
		crud_file_table[entry].object_id = crud_extract_oid( res );
	}

	// Set the position and the open bit	
	crud_file_table[entry].position = 0;
	crud_file_table[entry].open = 1;	

	print_table_entry( entry );
	return entry;
}
////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_close
// Description  : This function closes the file
//
// Inputs       : fd - the file handle of the object to close
// Outputs      : 0 if successful, -1 if failure

int16_t crud_close(int16_t fh) 
{
	// Make sure the device is mounted
	if( !mounted )
	{
		logMessage( LOG_ERROR_LEVEL, "The device is not mounted!" );
		return (-1);
	}
	
	// The valid bounds for a fh
	if( fh > CRUD_MAX_TOTAL_FILES || fh <= -1 ) 
	{
		logMessage( LOG_ERROR_LEVEL, "Invalid File Handler!\n" );
		return (-1);
	}

	// We can't close a non-opened file!
	if( crud_file_table[fh].open == 0 )
	{
		logMessage( LOG_ERROR_LEVEL, "Cannot close a non-opened file!" );
		return (-1);
	}

	// Set the open flag to closed
	crud_file_table[fh].open = 0;
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_read
// Description  : Reads up to "count" bytes from the file handle "fh" into the
//                buffer  "buf".
//
// Inputs       : fd - the file descriptor for the read
//                buf - the buffer to place the bytes into
//                count - the number of bytes to read
// Outputs      : the number of bytes read or -1 if failures

int32_t crud_read(int16_t fd, void *buf, int32_t count)
{
	// Sanity checks
	if( !mounted )
	{
		logMessage( LOG_ERROR_LEVEL, "The device is not mounted!" );
		return (-1);
	}

	if( !initialized )
	{
		logMessage( LOG_ERROR_LEVEL, "Object store has not been initialized in read()!\n" );
		return (-1);
	}

	if( fd > CRUD_MAX_TOTAL_FILES || fd <= -1 )
	{
		logMessage( LOG_ERROR_LEVEL, "Bad FD sent to read()!\n" );
		return (-1);
	}
	
	if( buf == NULL )
	{
		logMessage( LOG_ERROR_LEVEL, "NULL Buffer sent to read()!\n" );
		return (-1);
	}

	if( count < 0 )
	{
		logMessage( LOG_ERROR_LEVEL, "Count is negative! in read()!\n" );
		return (-1);
	}

	if( count > CRUD_MAX_OBJECT_SIZE )
	{
		logMessage( LOG_ERROR_LEVEL, "Count is greater than the max object size in read()!\n" );
		return (-1);
	}

	// Response from the device bus, buffer for the object and the result to return
	CrudResponse res = 0;
	char * readBuffer = malloc( CRUD_MAX_OBJECT_SIZE );
	uint32_t result;

	// Short hand for the table entry
	CrudFileAllocationType * entry = &crud_file_table[fd];

	// Read the current object
	res = crud_bus_request( crud_create_request_read( entry->object_id, CRUD_NULL_FLAG ), readBuffer );
	if( crud_operation_success( res ) == CRUD_FAILURE )
	{
		logMessage( LOG_ERROR_LEVEL, "Failed to read object in read()!\n" );
		return (-1);
	}

	// Make sure we have enough space to read the specified number of bytes
	if( entry->position + count > entry->length )
	{
		// Copy as many bytes as possible
		memcpy( buf, &readBuffer[entry->position], entry->length - entry->position );
		result = entry->length - entry->position;
		entry->position = entry->length;
	}
	else
	{
		// Copy all bytes specified
		memcpy( buf, &readBuffer[entry->position], count );
		entry->position = entry->position + count;
		result = count;
	}

	// Make sure to free the buffer
	free( readBuffer );
	return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_write
// Description  : Writes "count" bytes to the file handle "fh" from the
//                buffer  "buf"
//
// Inputs       : fd - the file descriptor for the file to write to
//                buf - the buffer to write
//                count - the number of bytes to write
// Outputs      : the number of bytes written or -1 if failure

int32_t crud_write(int16_t fd, void *buf, int32_t count) 
{
	CrudFileAllocationType * entry = &crud_file_table[fd];

	// Sanity checks
	if( !mounted )
	{
		logMessage( LOG_ERROR_LEVEL, "The device is not online!" );
		return (-1);
	}

	if( buf == NULL )
	{
		logMessage( LOG_ERROR_LEVEL, "Buffer passed to write() is NULL!\n");
		return (-1);
	}

	if( count < 0 ) 
	{
		logMessage( LOG_ERROR_LEVEL, "The number of bytes requested to be written is negative.\n");
		return (-1);
	}

	if( count == 0 )
	{
		// Nothing to see here
		return 0;
	}
	
	if( fd > CRUD_MAX_TOTAL_FILES || fd <= (-1) )
	{
		logMessage( LOG_ERROR_LEVEL, "Invalid file descriptor of %d.\n", fd);
		return (-1);
	}

	if( entry->position + count > CRUD_MAX_OBJECT_SIZE )
	{
		logMessage( LOG_ERROR_LEVEL, "Trying to write a file larger than the max object size!\n" );
		return (-1);
	}

	// Buffer to copy the object, the original size
	CrudResponse uRes, rRes, cRes;
	char * readBuf = malloc( CRUD_MAX_OBJECT_SIZE );
	uint32_t originalSize, newSize;

	// First read the original buffer
	rRes = crud_bus_request( crud_create_request_read( entry->object_id, CRUD_NULL_FLAG ), readBuf );
	
	if( crud_operation_success( rRes ) == CRUD_SUCCESS )
	{
		// Next save the size of the original object
		originalSize = crud_extract_size( rRes );

		// Copy the data over to the read buffer
		memcpy( &readBuf[entry->position], buf, count );

		// Determine if a new object needs to be created
		if( entry->position + count > originalSize )
		{
			newSize = entry->position + count;

			// Create a new object
			cRes = crud_bus_request( crud_create_request_create( newSize, CRUD_NULL_FLAG ), readBuf );
			if( crud_operation_success( cRes ) == CRUD_SUCCESS )
			{
				// Delete the old object and save the new one
				crud_bus_request( crud_create_request_delete( entry->object_id, CRUD_NULL_FLAG ), NULL );
				entry->object_id = crud_extract_oid( cRes );
				entry->position = newSize;
				entry->length = newSize;
			}
			else 
			{
				// The create failed
				logMessage( LOG_ERROR_LEVEL, "We failed to create a new object with size %d.\n", newSize );

				// We need to free the buffer though
				free( readBuf );
				return (-1);
			}

		}
		else
		{
			// Just make an update - size stays the same
			uRes = crud_bus_request( crud_create_request_update( entry->object_id, originalSize, CRUD_NULL_FLAG ), readBuf );
			if( crud_operation_success( uRes ) == CRUD_FAILURE )
			{
				logMessage( LOG_ERROR_LEVEL, "Update request failed!\n" );
				return (-1);
			}
			
			entry->position = entry->position + count;
		}

		// We need to free the buffer
		free( readBuf );
		return count;
	}
	// The read failed
	else
	{
		logMessage( LOG_ERROR_LEVEL, "ERROR: First read in write() failed.\n");

		// We need to free the buffer
		free( readBuf );
		return (-1);
	}
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_seek
// Description  : Seek to specific point in the file
//
// Inputs       : fd - the file descriptor for the file to seek
//                loc - offset from beginning of file to seek to
// Outputs      : 0 if successful or -1 if failure

int32_t crud_seek(int16_t fd, uint32_t loc) 
{
	CrudFileAllocationType * entry = &crud_file_table[fd];

	// Make sure the object store has been initialized
	if( !mounted )
	{
		logMessage( LOG_ERROR_LEVEL, "The device is not mounted!" );
		return (-1);
	}

	// Make sure the fd is valid
	if( fd > CRUD_MAX_TOTAL_FILES || fd <= (-1) )
	{
		logMessage( LOG_ERROR_LEVEL, "Bad FD in seek()!\n" );
		return (-1);
	}

	// Make sure that the new loc is within the file - strict comparison
	if( loc > entry->length )
	{
		logMessage( LOG_ERROR_LEVEL, "New location outside of size in seek()! %d >= %d\n", loc, entry->length );
		return (-1);
	}

	entry->position = loc;
	return 0;
}

// *** END ASSIGNMENT 3 CODE ***

// Module local methods

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crudIOUnitTest
// Description  : Perform a test of the CRUD IO implementation
//
// Inputs       : None
// Outputs      : 0 if successful or -1 if failure

int crudIOUnitTest(void) {

	// Local variables
	uint8_t ch;
	int16_t fh, i;
	int32_t cio_utest_length, cio_utest_position, count, bytes, expected;
	char *cio_utest_buffer, *tbuf;
	CRUD_UNIT_TEST_TYPE cmd;
	char lstr[1024];

	// Setup some operating buffers, zero out the mirrored file contents
	cio_utest_buffer = malloc(CRUD_MAX_OBJECT_SIZE);
	tbuf = malloc(CRUD_MAX_OBJECT_SIZE);
	memset(cio_utest_buffer, 0x0, CRUD_MAX_OBJECT_SIZE);
	cio_utest_length = 0;
	cio_utest_position = 0;

	// Format and mount the file system
	if (crud_format() || crud_mount()) {
		logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : Failure on format or mount operation.");
		return(-1);
	}

	// Start by opening a file
	fh = crud_open("temp_file.txt");
	if (fh == -1) {
		logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : Failure open operation.");
		return(-1);
	}

	// Now do a bunch of operations
	for (i=0; i<CRUD_IO_UNIT_TEST_ITERATIONS; i++) {

		// Pick a random command
		if (cio_utest_length == 0) {
			cmd = CIO_UNIT_TEST_WRITE;
		} else {
			cmd = getRandomValue(CIO_UNIT_TEST_READ, CIO_UNIT_TEST_SEEK);
		}

		// Execute the command
		switch (cmd) {

		case CIO_UNIT_TEST_READ: // read a random set of data
			count = getRandomValue(0, cio_utest_length);
			logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : read %d at position %d", bytes, cio_utest_position);
			bytes = crud_read(fh, tbuf, count);
			if (bytes == -1) {
				logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : Read failure.");
				return(-1);
			}

			// Compare to what we expected
			if (cio_utest_position+count > cio_utest_length) {
				expected = cio_utest_length-cio_utest_position;
			} else {
				expected = count;
			}
			if (bytes != expected) {
				logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : short/long read of [%d!=%d]", bytes, expected);
				return(-1);
			}
			if ( (bytes > 0) && (memcmp(&cio_utest_buffer[cio_utest_position], tbuf, bytes)) ) {

				bufToString((unsigned char *)tbuf, bytes, (unsigned char *)lstr, 1024 );
				logMessage(LOG_INFO_LEVEL, "CIO_UTEST R: %s", lstr);
				bufToString((unsigned char *)&cio_utest_buffer[cio_utest_position], bytes, (unsigned char *)lstr, 1024 );
				logMessage(LOG_INFO_LEVEL, "CIO_UTEST U: %s", lstr);

				logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : read data mismatch (%d)", bytes);
				return(-1);
			}
			logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : read %d match", bytes);


			// update the position pointer
			cio_utest_position += bytes;
			break;

		case CIO_UNIT_TEST_APPEND: // Append data onto the end of the file
			// Create random block, check to make sure that the write is not too large
			ch = getRandomValue(0, 0xff);
			count =  getRandomValue(1, CIO_UNIT_TEST_MAX_WRITE_SIZE);
			if (cio_utest_length+count >= CRUD_MAX_OBJECT_SIZE) {

				// Log, seek to end of file, create random value
				logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : append of %d bytes [%x]", count, ch);
				logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : seek to position %d", cio_utest_length);
				if (crud_seek(fh, cio_utest_length)) {
					logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : seek failed [%d].", cio_utest_length);
					return(-1);
				}
				cio_utest_position = cio_utest_length;
				memset(&cio_utest_buffer[cio_utest_position], ch, count);

				// Now write
				bytes = crud_write(fh, &cio_utest_buffer[cio_utest_position], count);
				if (bytes != count) {
					logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : append failed [%d].", count);
					return(-1);
				}
				cio_utest_length = cio_utest_position += bytes;
			}
			break;

		case CIO_UNIT_TEST_WRITE: // Write random block to the file
			ch = getRandomValue(0, 0xff);
			count =  getRandomValue(1, CIO_UNIT_TEST_MAX_WRITE_SIZE);
			// Check to make sure that the write is not too large
			if (cio_utest_length+count < CRUD_MAX_OBJECT_SIZE) {
				// Log the write, perform it
				logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : write of %d bytes [%x]", count, ch);
				memset(&cio_utest_buffer[cio_utest_position], ch, count);
				bytes = crud_write(fh, &cio_utest_buffer[cio_utest_position], count);
				if (bytes!=count) {
					logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : write failed [%d].", count);
					return(-1);
				}
				cio_utest_position += bytes;
				if (cio_utest_position > cio_utest_length) {
					cio_utest_length = cio_utest_position;
				}
			}
			break;

		case CIO_UNIT_TEST_SEEK:
			count = getRandomValue(0, cio_utest_length);
			logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : seek to position %d", count);
			if (crud_seek(fh, count)) {
				logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : seek failed [%d].", count);
				return(-1);
			}
			cio_utest_position = count;
			break;

		default: // This should never happen
			CMPSC_ASSERT0(0, "CRUD_IO_UNIT_TEST : illegal test command.");
			break;

		}

#if DEEP_DEBUG
		// VALIDATION STEP: ENSURE OUR LOCAL IS LIKE OBJECT STORE
		CrudRequest request;
		CrudResponse response;
		CrudOID oid;
		CRUD_REQUEST_TYPES req;
		uint32_t length;
		uint8_t res, flags;

		// Make a fake request to get file handle, then check it
		request = construct_crud_request(crud_file_table[0].object_id, CRUD_READ, CRUD_MAX_OBJECT_SIZE, CRUD_NULL_FLAG, 0);
		response = crud_bus_request(request, tbuf);
		if ((deconstruct_crud_request(response, &oid, &req, &length, &flags, &res) != 0) || (res != 0))  {
			logMessage(LOG_ERROR_LEVEL, "Read failure, bad CRUD response [%x]", response);
			return(-1);
		}
		if ( (cio_utest_length != length) || (memcmp(cio_utest_buffer, tbuf, length)) ) {
			logMessage(LOG_ERROR_LEVEL, "Buffer/Object cross validation failed [%x]", response);
			bufToString((unsigned char *)tbuf, length, (unsigned char *)lstr, 1024 );
			logMessage(LOG_INFO_LEVEL, "CIO_UTEST VR: %s", lstr);
			bufToString((unsigned char *)cio_utest_buffer, length, (unsigned char *)lstr, 1024 );
			logMessage(LOG_INFO_LEVEL, "CIO_UTEST VU: %s", lstr);
			return(-1);
		}

		// Print out the buffer
		bufToString((unsigned char *)cio_utest_buffer, cio_utest_length, (unsigned char *)lstr, 1024 );
		logMessage(LOG_INFO_LEVEL, "CIO_UTEST: %s", lstr);
#endif

	}

	// Close the files and cleanup buffers, assert on failure
	if (crud_close(fh)) {
		logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : Failure read comparison block.", fh);
		return(-1);
	}
	free(cio_utest_buffer);
	free(tbuf);

	// Format and mount the file system
	if (crud_unmount()) {
		logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : Failure on unmount operation.");
		return(-1);
	}

	// Return successfully
	return(0);
}

