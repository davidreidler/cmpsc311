////////////////////////////////////////////////////////////////////////////////
//
//  File          : cmpsc311-f14-assign2.c
//  Description   : This is the main code file for the CMPSC311 assignment 2.
//                  see class assignment for details on what needs to be added.
//
//  Author        : David Reidler
//  Created       : September 12, 2014
//
#include <math.h>
#include <stdio.h>
#include <stddef.h>
#include "a2support.h"

#define NUMBER_ENTRIES 20

////////////////////////////////////////////////////////////////////////////////
//
// Function     : main
// Description  : This is the main function for the cmpsc311-f13-assign2 program.
//
// Inputs       : none
// Outputs      : 0 if successful, -1 otherwise

int main( void ) {

	// Local variables
	int integerArray[NUMBER_ENTRIES], i, j;
	float floatArray[NUMBER_ENTRIES];
	char * tmpStr = NULL;

	// Read the integer values
	for ( i=0; i<NUMBER_ENTRIES; i++ ) {
	    scanf( "%d", &integerArray[i] );
	}

   	/* 2: Create an array of floats: ODD indicies should be calculated with
		 sin() and the respective integer in integerArray            */
	for( j = 0; j < NUMBER_ENTRIES; j++ )
	{
		if( j % 2 == 1)
			floatArray[j] = sin( integerArray[j] );
		else floatArray[j] = cos( integerArray[j] );
	}

	/* 3:	Print out the integers and floats with showIntegers() and 
		show floats()  */
	showIntegers( integerArray, NUMBER_ENTRIES );
	showFloats( floatArray, NUMBER_ENTRIES );

	/* 4:	Print out the number of bits for each integer */
	for( i = 0; i < NUMBER_ENTRIES; i++ )
	{
		printf( "The integer array value %x has %2d bits.\n", integerArray[i], countBits( integerArray[i] ) );
	}

	/* 5:	Sort each array with integerQuickSort() and floatQuickSort() */
	integerQuickSort( integerArray, 0, NUMBER_ENTRIES - 1 );
	floatQuickSort( floatArray, 0, NUMBER_ENTRIES - 1 );

	printf( "The sorted integers are: \n" );
	showIntegers( integerArray, NUMBER_ENTRIES );

	printf( "The sorted floats are: \n" );
	showFloats( floatArray, NUMBER_ENTRIES );

	/* 6:	Print out the median value of each array using medianInteger()
		and medianFloat() */
	printf( "The median of the integers is %.*f.\n", 2, medianInteger( integerArray, NUMBER_ENTRIES ) );
	printf( "The median of the floats is %.*f.\n", 2, medianFloat( floatArray, NUMBER_ENTRIES ) );

	/* 7:	Cast each integer to an unsigned short and compute the number
		with its bits reversed. Print out the binary representation 
		using binaryString() */
	for( i = 0; i < NUMBER_ENTRIES; i++ ) 
	{
		printf("The unsigned short value 0x%x\n", (unsigned short) integerArray[i] );

		binaryString( &tmpStr, sizeof(unsigned short) * 8, (unsigned short) integerArray[i] );
		printf("  ORIG : %s\n", tmpStr );

		binaryString( &tmpStr, sizeof(unsigned short) * 8, reverseBits( (unsigned short) integerArray[i] ) );
		printf("  REVR : %s\n", tmpStr );
	}

	/* 8:	Print out CDF plot. */	
	showCDF( integerArray, NUMBER_ENTRIES );

	// Return successfully
	return( 1 );
}
