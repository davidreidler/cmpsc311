////////////////////////////////////////////////////////////////////////////////
//
//  File          : crud_client.c
//  Description   : This is the client side of the CRUD communication protocol.
//
//  Author        : Patrick McDaniel
//  Last Modified : Thu Oct 30 06:59:59 EDT 2014
//

// Include Files

#include <stdint.h>
#include <unistd.h>

// Project Include Files
#include <crud_driver.h>
#include <crud_network.h>
#include <cmpsc311_log.h>
#include <cmpsc311_util.h>
#include <crud_request_utils.h>

// Network Defines
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

// Global variables
int            crud_network_shutdown = 0;                 // Flag indicating shutdown
unsigned char *crud_network_address  = CRUD_DEFAULT_IP;   // Address of CRUD server 
unsigned short crud_network_port     = CRUD_DEFAULT_PORT; // Port of CRUD server

int            connected             = 0;                 // Flag indicating last successful read/write on the network 
int            sockfd                = -1;                // File Descriptor for our socket

//
// Functions

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_client_operation
// Description  : This the client operation that sends a request to the CRUD
//                server.   It will:
//
//                1) if INIT make a connection to the server
//                2) send any request to the server, returning results
//                3) if CLOSE, will close the connection
//
// Inputs       : op - the request opcode for the command
//                buf - the block to be read/written from (READ/WRITE)
// Outputs      : the response structure encoded as needed

CrudResponse crud_client_operation( CrudRequest op, void *buf )
{
	// Response value returned from the server
	CrudResponse response;

	// Connect if this is the first time we're connecting
	if( crud_extract_op( op ) == CRUD_INIT || !connected )
	{
		if( crud_network_connect() == -1 )
		{
			// Set the success bit in the return value
			logMessage( LOG_ERROR_LEVEL, "Failed to connect to the server!" );
			return (op | 1);
		}
	}

	// Send the request to the server and recieve it's response - (-1) means failure
	if( crud_network_send( op, buf ) == (-1) )
	{
		logMessage( LOG_ERROR_LEVEL, "Failed to send the whole request to the server in network_send()!" );
		return (op | 1);
	}

	// Recieve the server's response - crud_operation_success() returns the result bit in the response
	response = crud_network_recieve( op, buf );
	if( crud_operation_success( response ) == 1 )
	{
		logMessage( LOG_ERROR_LEVEL, "Failed to recieve the server's response in network_recieve()!" );
		return (response | 1);
	}

	// For a CRUD_CLOSE, disconnect from the server and reset flag
	if( crud_extract_op( op ) == CRUD_CLOSE && connected )
	{
		close( sockfd );
		sockfd = -1;
		connected = 0;
	}

	return response;
}


////////////////////////////////////////////////////////////////////////////////
// 
//  Function    : crud_network_connect
//  Description : Creates a network connection to the CRUD server.
//
//  Input       : N/A
//  Returns     : 0 on success, -1 on failure

int crud_network_connect( void ) 
{
	// A result number indicating success or failure and address information for the connection
	// NOTE: We are being friendly to IPv6 because it's IPv6 and we should support it you know?
	int res = 0;
	struct addrinfo hints, * info;

	// Zero-out struct and fill out known info for the server
	memset( &hints, 0, sizeof( hints ) );
	hints.ai_family   = AF_UNSPEC;            // The OS can figure this out - let it
	hints.ai_socktype = SOCK_STREAM;

	// Fill in our result - IPv6 Friendly!
	// NOTE: We need a string for the port in getaddrinfo()...therefore it's hardcoded
	res = getaddrinfo( CRUD_DEFAULT_IP, "19876", &hints, &info );

	// Returns 0 on success - other error codes is a fail
	if( res != 0 )
	{
		logMessage( LOG_ERROR_LEVEL, "Failed to get addrinfo in crud_network_connect()!" );
		return (-1);
	}

	// Create our file descriptor
	sockfd = socket( info->ai_family, info->ai_socktype, info->ai_protocol );
	if( sockfd == -1 )
	{
		logMessage( LOG_ERROR_LEVEL, "Failed to open a socket in crud_network_connect()" );
		return (-1);
	}

	// Connect to the server
	res = connect( sockfd, info->ai_addr, info->ai_addrlen );
	if( res != 0 )
	{
		logMessage( LOG_ERROR_LEVEL, "Failed to connect to the server in crud_network_connect()!" );
		return (-1);
	}

	connected = 1;
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_network_send
//  Description : Takes a CRUD opcode and optionally a buffer, and sends them to
//                the CRUD server
//
//  Input       : CRUD opcode, memory block to send to the CRUD server.
//  Returns     : Returns 0 on success, -1 on failure.

int crud_network_send( CrudRequest op, void * buf )
{
	// Length of the object in the store and the server's response
	int len = crud_extract_size( op ), bytesSent = 0;
	uint64_t value;

	// Make sure we are connected
	if( !connected )
	{
		if( crud_network_connect() == -1 )
		{
			logMessage( LOG_ERROR_LEVEL, "Failed to connect to server in crud_network_send()!" );
			return (-1);
		}
	}

	// Convert the opcode to network-byte order
	value = htonll64( op );

	// Send the converted opcode to the server
	// NOTE: Send doesn't always send all the data at once, but since sizeof( value ) is < 1KB, it should send in one go.
	if( send( sockfd, &value, sizeof( value ), 0 ) != sizeof( value ) )
	{
		logMessage( LOG_ERROR_LEVEL, "There was an error in the number of bytes written to the network in crud_network_send()!" );
		return (-1);
	}

	// Send the buffer if it's a create or update
	if( crud_extract_op( op ) == CRUD_CREATE || crud_extract_op( op ) == CRUD_UPDATE )
	{
		// Make sure that the entire buffer is written over the wire
		while( bytesSent != len )
		{
			bytesSent = bytesSent + send( sockfd, buf + bytesSent, len - bytesSent, 0 );
			if( bytesSent == -1 )
			{
				logMessage( LOG_ERROR_LEVEL, "There was a problem sending the data to the server!" );
				close( sockfd );
				sockfd = -1;
				connected = 0;
				return 1;
			}
		}
	}

	return (0);
}


////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_network_recieve
//  Description : Recieves the response from the CRUD server after the request
//                is sent to the server. Copies any additional data into a
//                memory block.
//
//  Input       : CRUD opcode that was sent and an address to a memory block to
//                copy additional data sent from the CRUD server.
//  Returns     : Returns the CrudResponse sent by the server.

CrudResponse crud_network_recieve( CrudRequest op, void * buf )
{
	// The CrudResponse from the server and the number of bytes read and to be read
	int bytesRead = 0, bytesSent = 0;
	uint64_t rec, res;

	// Make sure we are connected to the server
	if( !connected )
	{
		logMessage( LOG_ERROR_LEVEL, "Not connected in crud_network_recieve()!" );
		return 1;
	}

	// Recieve the CrudResponse from the server
	// NOTE: recv() might not send all the data, but since sizeof( rec ) < 1KB it should send it all without a problem
	if( recv( sockfd, &rec, sizeof( rec ), 0 ) == -1 )
	{
		logMessage( LOG_ERROR_LEVEL, "There was a problem recieveing the server's response." );

		// Failed to read - disconnect
		close( sockfd );
		connected = 0;
		sockfd = -1;
		return 1;
	}

	// Convert response value to host-byte order
	res = ntohll64( rec );

	// Extract request type and length from response - Make sure the request was completed successfully.
	if( crud_extract_op( res ) == CRUD_READ && crud_operation_success( res ) == 0 )
	{
		// Make sure we recieve all of the data
		bytesSent = crud_extract_size( res );
		while( bytesRead != bytesSent )
		{
			bytesRead = bytesRead + recv( sockfd, buf + bytesRead, bytesSent - bytesRead, 0 );
			if( bytesRead == -1)
			{
				// Error and disconnect
				logMessage( LOG_ERROR_LEVEL, "Something happened and we couldn't read from the server" );
				close( sockfd );
				connected = 0;
				sockfd = -1;
				return 1;
			}
		}
	}

	return res;
}
