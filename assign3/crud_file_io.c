////////////////////////////////////////////////////////////////////////////////
//
//  File           : crud_file_io.h
//  Description    : This is the implementation of the standardized IO functions
//                   for used to access the CRUD storage system.
//
//  Author         : Patrick McDaniel
//  Last Modified  : Tue Sep 16 19:38:42 EDT 2014
//

// Includes
#include <malloc.h>
#include <string.h>

// Project Includes
#include <crud_driver.h>
#include <crud_file_io.h>
#include <cmpsc311_log.h>
#include <cmpsc311_util.h>


//
// Defines 
#define CIO_UNIT_TEST_MAX_WRITE_SIZE 1024
#define CRUD_IO_UNIT_TEST_ITERATIONS 10240

#define CRUD_SUCCESS 0
#define CRUD_FAILURE 1

// Type for UNIT test interface
typedef enum {
    CIO_UNIT_TEST_READ   = 0,
    CIO_UNIT_TEST_WRITE  = 1,
    CIO_UNIT_TEST_APPEND = 2,
    CIO_UNIT_TEST_SEEK   = 3,
} CRUD_UNIT_TEST_TYPE;

//
// Implementation

// Globals
CrudOID currentOID;
uint16_t currentFD;
uint32_t currentPOS;
int initialized = 0;
uint32_t fileSize;

//
// CRUD Request/Response Utilities

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_create_request_init
//  Description : Creates an initialization request to send to the object store
//
//  Arguments   : None - A request of 0 is an initialization request
//  Returns     : A CrudRequest (uint64) which has been modified to meet the 
//                request structure.
CrudRequest crud_create_request_init()
{
	// By the request scheme this is an initialization request
	CrudRequest ret = 0;
	return ret;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_create_request_create
//  Description : Creates a CRUD request to create an object whose length is
//                is defined in the request.
//
//  Arguments   : Length of the buffer that will be used to in the request.
//                Since this is a create request, we don't need an OID.
//  Returns     : A CrudRequest representing a create command that can be sent
//                to the object store.
CrudRequest crud_create_request_create( uint32_t len )
{
	CrudRequest res = 0;

	// Shift the CRUD_CREATE enum over by 28 bits
	// cast it to make sure it has 64 bits
	res = ( (uint64_t)CRUD_CREATE << 28 );

	// The & operation zero-outs the 4 most significant bits (just in-case)
	res = res + ( ( len & CRUD_MAX_OBJECT_SIZE ) << 4 );
	return res;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_create_request_read
//  Description : This function creates a read request to send to the object
//                store by structuring it in accordance with the object request
//                protocol.
//
//  Arugments   : The CRUD Object ID that is to be read
//  Returns     : A CrudRequest representing a read command that can be sent to
//                the object store.
CrudRequest crud_create_request_read( CrudOID oid )
{
	// CrudOID is 32 bits long, we need at least  64 for the shift 
	CrudRequest res = (uint64_t) oid << 32 ;

	// Insert the read command
	res = res + ( (uint64_t)CRUD_READ << 28 );

	// For reads, we won't know how long the file is exactly, so we should
	// use the max object size
	res = res + ( (uint64_t)CRUD_MAX_OBJECT_SIZE << 4 );
	return res;
}

////////////////////////////////////////////////////////////////////////////////
//  
//  Function    : crud_create_request_update
//  Description : Creates a CRUD request to update a specific object based on
//                the Object's ID.
//
//  Arguments   : The object's ID and the the length of the object.
//  Returns     : A CrudRequest that represents an update command that can be 
//                sent to the object store 
CrudRequest crud_create_request_update( CrudOID oid, uint32_t len )
{
	// CrudOID is 32 bits long, we need at least 64 for the shift
	CrudRequest res = (uint64_t)oid << 32 ;

	// Insert the update command
	res = res + ( (uint64_t)CRUD_UPDATE << 28 );

	// Insert the size
	// The & zeros-out the 8 most significant bits of len (just incase)
	res = res + ( ( len & CRUD_MAX_OBJECT_SIZE ) << 4 );
	return res;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_create_request_delete
//  Description : Creates a CrudRequest that represents a delete command on the
//                specified object ID.
//
//  Arguments   : The object ID of the specified object.
//  Returns     : A CrudRequest that represents a delete command that can be
//                sent to the object store.
CrudRequest crud_create_request_delete( CrudOID oid )
{
	// CrudIOD is 32 bits long, we need at least 64 for the shift
	CrudRequest res = (uint64_t)oid << 32;

	// Insert the delete command
	res = res + ( (uint64_t)CRUD_DELETE << 28 );
	return res;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_operation_success
//  Description : Determines if a CRUD operation was successful based on the
//                response code that was received.
//
//  Arguments   : A CRUD response code (hopefully) received by the object store
//  Returns     : 0 if successful, 1 if not successful.
uint64_t crud_operation_success( CrudResponse res )
{
	if( res & 1 ) return CRUD_FAILURE; 
	else return CRUD_SUCCESS; 
}

////////////////////////////////////////////////////////////////////////////////
//
//  Function    : crud_extract_oid
//  Description : Takes a CrudResponse and extracts the Object ID from it.
//
//  Arguments   : The CRUD response from the object store.
//  Returns     : The Object ID in the response.
//
CrudOID  crud_extract_oid( CrudResponse res )
{
	// Result variable
	CrudOID result;

	// Set the result to the most significant 32 bits
	// Since the response is 64 bits, we need to shift 32 bits right
	result = (CrudOID)( res >> 32 );

	return result;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_extract_size
// Description  : This function returns the size of an object from a 
//                CrudResponse.
//
// Inputs       : res - the CrudResponse
// Outputs      : The size of the object in the CrudResponse.
uint32_t crud_extract_size( CrudResponse res )
{
	uint32_t result;

	// Shift right and make sure it has the right bits set
	result = (res >> 4) & (CRUD_MAX_OBJECT_SIZE);
	return result;
}

//
// CRUD File I/O

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_open
// Description  : This function opens the file and returns a file handle
//
// Inputs       : path - the path "in the storage array"
// Outputs      : file handle if successful, -1 if failure

int16_t crud_open(char *path) 
{

	CrudResponse res;

	// Make sure that the object store has been initialized
	if ( !initialized )
	{
		// We don't fail here and continue on
		crud_bus_request( crud_create_request_init(), NULL );
		initialized = 1;
	}

	// Kinda not needed for this assignment, but shouldn't be ignored
	if( path == NULL )
	{
		logMessage( LOG_ERROR_LEVEL, "path is NULL in open!\n" );
		return (-1);
	}

	// We should assume the file is 0 length
	res = crud_bus_request( crud_create_request_create( 0 ), NULL );

	if( crud_operation_success( res ) == CRUD_SUCCESS )
	{
		// Set the new OID to the OID in the result
		currentOID = crud_extract_oid( res );

		// NOTE: There is only one file open at any time, so it is safe
		// to use the same fd for all files (for this assignment)
		currentFD = 22;

		// Reset the current position if it was already modified
		currentPOS = 0;

		fileSize = 0;

		return currentFD;
	}
	else return (-1);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_close
// Description  : This function closes the file
//
// Inputs       : fd - the file handle of the object to close
// Outputs      : 0 if successful, -1 if failure

int16_t crud_close(int16_t fh) 
{
	CrudResponse res;

	// Make sure the object store has been initialized
	if( !initialized )
	{
		logMessage( LOG_ERROR_LEVEL, "Object store has not been initialized in close()!\n" );
		return (-1);
	}
	
	// If the file handle doesn't exist then we fail
	if( fh != currentFD ) 
	{
		logMessage( LOG_ERROR_LEVEL, "Invalid File Handler!\n" );
		return (-1);
	}

	// Send the delete command with the oid we are looking to delete
	res = crud_bus_request( crud_create_request_delete( currentOID ), NULL );
	if( crud_operation_success( res ) == CRUD_SUCCESS )
	{
		// Clear all of our globals
		currentFD = 0;
		currentPOS = 0;
		currentOID = 0;
		initialized = 0;
		fileSize = 0;
		return 0;
	}
	else return (-1);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_read
// Description  : Reads up to "count" bytes from the file handle "fh" into the
//                buffer  "buf".
//
// Inputs       : fd - the file descriptor for the read
//                buf - the buffer to place the bytes into
//                count - the number of bytes to read
// Outputs      : the number of bytes read or -1 if failures

int32_t crud_read(int16_t fd, void *buf, int32_t count)
{

	// Sanity checks
	if( !initialized )
	{
		logMessage( LOG_ERROR_LEVEL, "Object store has not been initialized in read()!\n" );
		return (-1);
	}

	if( fd != currentFD )
	{
		logMessage( LOG_ERROR_LEVEL, "Bad FD sent to read()!\n" );
		return (-1);
	}
	
	if( buf == NULL )
	{
		logMessage( LOG_ERROR_LEVEL, "NULL Buffer sent to read()!\n" );
		return (-1);
	}

	if( count < 0 )
	{
		logMessage( LOG_ERROR_LEVEL, "Count is negative! in read()!\n" );
		return (-1);
	}

	if( count > CRUD_MAX_OBJECT_SIZE )
	{
		logMessage( LOG_ERROR_LEVEL, "Count is greater than the max object size in read()!\n" );
		return (-1);
	}

	// Response from the device bus, buffer for the object and the result to return
	CrudResponse res = 0;
	char * readBuffer = malloc( CRUD_MAX_OBJECT_SIZE );
	uint32_t result;

	// Read the current object
	res = crud_bus_request( crud_create_request_read( currentOID ), readBuffer );

	if( crud_operation_success( res ) == CRUD_FAILURE )
	{
		logMessage( LOG_ERROR_LEVEL, "Failed to read object in read()!\n" );
		return (-1);
	}

	// Make sure we have enough space to read the specified number of bytes
	if( currentPOS + count > fileSize )
	{
		// Copy as many bytes as possible
		memcpy( buf, &readBuffer[currentPOS], fileSize - currentPOS );
		result = fileSize - currentPOS;
		currentPOS = fileSize;
	}
	else
	{
		// Copy all bytes specified
		memcpy( buf, &readBuffer[currentPOS], count );
		currentPOS = currentPOS + count;
		result = count;
	}

	// Make sure to free the buffer
	free( readBuffer );
	return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_write
// Description  : Writes "count" bytes to the file handle "fh" from the
//                buffer  "buf"
//
// Inputs       : fd - the file descriptor for the file to write to
//                buf - the buffer to write
//                count - the number of bytes to write
// Outputs      : the number of bytes written or -1 if failure

int32_t crud_write(int16_t fd, void *buf, int32_t count) 
{
	// Sanity checks
	if( !initialized )
	{
		logMessage( LOG_ERROR_LEVEL, "Object store has not been initialized, in write()!\n" );
		return (-1);
	}

	if( buf == NULL )
	{
		logMessage( LOG_ERROR_LEVEL, "Buffer passed to write() is NULL!\n");
		return (-1);
	}

	if( count < 0 ) 
	{
		logMessage( LOG_ERROR_LEVEL, "The number of bytes requested to be written is negative.\n");
		return (-1);
	}

	if( count == 0 )
	{
		// Nothing to see here
		return 0;
	}
	
	if( fd != currentFD )
	{
		logMessage( LOG_ERROR_LEVEL, "Invalid file descriptor of %d.\n", fd);
		return (-1);
	}

	if( currentPOS + count > CRUD_MAX_OBJECT_SIZE )
	{
		logMessage( LOG_ERROR_LEVEL, "Trying to write a file larger than the max object size!\n" );
		return (-1);
	}

	// Buffer to copy the object, the original size
	CrudResponse uRes, rRes, cRes;
	char * readBuf = malloc( CRUD_MAX_OBJECT_SIZE );
	uint32_t originalSize, newSize;

	// First read the original buffer
	rRes = crud_bus_request( crud_create_request_read( currentOID ), readBuf );
	
	if( crud_operation_success( rRes ) == CRUD_SUCCESS )
	{
		// Next save the size of the original object
		originalSize = crud_extract_size( rRes);

		// Copy the data over to the read buffer
		memcpy( &readBuf[currentPOS], buf, count );

		// Determine if a new object needs to be created
		if( currentPOS + count > originalSize )
		{
			newSize = currentPOS + count;

			// Create a new object
			cRes = crud_bus_request( crud_create_request_create( newSize ), readBuf );
			if( crud_operation_success( cRes ) == CRUD_SUCCESS )
			{
				// Delete the old object and save the new one
				crud_bus_request( crud_create_request_delete( currentOID ), NULL );
				currentOID = crud_extract_oid( cRes );
				currentPOS = newSize;
				fileSize = newSize;
			}
			else 
			{
				// The create failed
				logMessage( LOG_ERROR_LEVEL, "We failed to create a new object with size %d.\n", newSize );

				// We need to free the buffer though
				free( readBuf );
				return (-1);
			}

		}
		else
		{
			// Just make an update - size stays the same
			uRes = crud_bus_request( crud_create_request_update( currentOID, originalSize ), readBuf );
			if( crud_operation_success( uRes ) == CRUD_FAILURE )
			{
				logMessage( LOG_ERROR_LEVEL, "Update request failed!\n" );
				return (-1);
			}
			
			currentPOS = currentPOS + count;
		}

		// We need to free the buffer
		free( readBuf );
		return count;
	}
	// The read failed
	else {
		logMessage( LOG_ERROR_LEVEL, "ERROR: First read in write() failed.\n");

		// We need to free the buffer
		free( readBuf );
		return (-1);
	}
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crud_seek
// Description  : Seek to specific point in the file
//
// Inputs       : fd - the file descriptor for the file to seek
//                loc - offset from beginning of file to seek to
// Outputs      : 0 if successful or -1 if failure

int32_t crud_seek(int16_t fd, uint32_t loc) 
{

	// Make sure the object store has been initialized
	if( !initialized )
	{
		logMessage( LOG_ERROR_LEVEL, "Object store not initialized in seek()!\n" );
		return (-1);
	}

	// Make sure the fd is valid
	if( fd != currentFD )
	{
		logMessage( LOG_ERROR_LEVEL, "Bad FD in seek()!\n" );
		return (-1);
	}

	// Make sure that the new loc is within the file
	// NOTE: We might need a strict comparison rather than a weak one
	if( loc >= fileSize )
	{
		logMessage( LOG_ERROR_LEVEL, "New location outside of size in seek()!\n" );
		return (-1);
	}

	currentPOS = loc;

	return 0;
}

//
// Unit Test Function

////////////////////////////////////////////////////////////////////////////////
//
// Function     : crudIOUnitTest
// Description  : Perform a test of the CRUD IO implementation
//
// Inputs       : None
// Outputs      : 0 if successful or -1 if failure

int crudIOUnitTest(void) {

	// Local variables
	uint8_t ch;
	int16_t fh, i;
	int32_t cio_utest_length, cio_utest_position, count, bytes, expected;
	char *cio_utest_buffer, *tbuf;
	CRUD_UNIT_TEST_TYPE cmd;
	char lstr[1024];

	// Setup some operating buffers, zero out the mirrored file contents
	cio_utest_buffer = malloc(CRUD_MAX_OBJECT_SIZE);
	tbuf = malloc(CRUD_MAX_OBJECT_SIZE);
	memset(cio_utest_buffer, 0x0, CRUD_MAX_OBJECT_SIZE);
	cio_utest_length = 0;
	cio_utest_position = 0;

	// Start by opening a file
	fh = crud_open("temp_file.txt");
	if (fh == -1) {
		logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : Failure open operation.");
		return(-1);
	}

	// Now do a bunch of operations
	for (i=0; i<CRUD_IO_UNIT_TEST_ITERATIONS; i++) {

		// Pick a random command
		if (cio_utest_length == 0) {
			cmd = CIO_UNIT_TEST_WRITE;
		} else {
			cmd = getRandomValue(CIO_UNIT_TEST_READ, CIO_UNIT_TEST_SEEK);
		}

		// Execute the command
		switch (cmd) {

		case CIO_UNIT_TEST_READ: // read a random set of data
			count = getRandomValue(0, cio_utest_length);
			logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : read %d at position %d", bytes, cio_utest_position);
			bytes = crud_read(fh, tbuf, count);
			if (bytes == -1) {
				logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : Read failure.");
				return(-1);
			}

			// Compare to what we expected
			if (cio_utest_position+count > cio_utest_length) {
				expected = cio_utest_length-cio_utest_position;
			} else {
				expected = count;
			}
			if (bytes != expected) {
				logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : short/long read of [%d!=%d]", bytes, expected);
				return(-1);
			}
			if ( (bytes > 0) && (memcmp(&cio_utest_buffer[cio_utest_position], tbuf, bytes)) ) {

				bufToString((unsigned char *)tbuf, bytes, (unsigned char *)lstr, 1024 );
				logMessage(LOG_INFO_LEVEL, "CIO_UTEST R: %s", lstr);
				bufToString((unsigned char *)&cio_utest_buffer[cio_utest_position], bytes, (unsigned char *)lstr, 1024 );
				logMessage(LOG_INFO_LEVEL, "CIO_UTEST U: %s", lstr);

				logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : read data mismatch (%d)", bytes);
				return(-1);
			}
			logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : read %d match", bytes);


			// update the position pointer
			cio_utest_position += bytes;
			break;

		case CIO_UNIT_TEST_APPEND: // Append data onto the end of the file
			// Create random block, check to make sure that the write is not too large
			ch = getRandomValue(0, 0xff);
			count =  getRandomValue(1, CIO_UNIT_TEST_MAX_WRITE_SIZE);
			if (cio_utest_length+count >= CRUD_MAX_OBJECT_SIZE) {

				// Log, seek to end of file, create random value
				logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : append of %d bytes [%x]", count, ch);
				logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : seek to position %d", cio_utest_length);
				if (crud_seek(fh, cio_utest_length)) {
					logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : seek failed [%d].", cio_utest_length);
					return(-1);
				}
				cio_utest_position = cio_utest_length;
				memset(&cio_utest_buffer[cio_utest_position], ch, count);

				// Now write
				bytes = crud_write(fh, &cio_utest_buffer[cio_utest_position], count);
				if (bytes != count) {
					logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : append failed [%d].", count);
					return(-1);
				}
				cio_utest_length = cio_utest_position += bytes;
			}
			break;

		case CIO_UNIT_TEST_WRITE: // Write random block to the file
			ch = getRandomValue(0, 0xff);
			count =  getRandomValue(1, CIO_UNIT_TEST_MAX_WRITE_SIZE);
			// Check to make sure that the write is not too large
			if (cio_utest_length+count < CRUD_MAX_OBJECT_SIZE) {
				// Log the write, perform it
				logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : write of %d bytes [%x]", count, ch);
				memset(&cio_utest_buffer[cio_utest_position], ch, count);
				bytes = crud_write(fh, &cio_utest_buffer[cio_utest_position], count);
				if (bytes!=count) {
					logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : write failed [%d].", count);
					return(-1);
				}
				cio_utest_position += bytes;
				if (cio_utest_position > cio_utest_length) {
					cio_utest_length = cio_utest_position;
				}
			}
			break;

		case CIO_UNIT_TEST_SEEK:
			count = getRandomValue(0, cio_utest_length);
			logMessage(LOG_INFO_LEVEL, "CRUD_IO_UNIT_TEST : seek to position %d", count);
			if (crud_seek(fh, count)) {
				logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : seek failed [%d].", count);
				return(-1);
			}
			cio_utest_position = count;
			break;

		default: // This should never happen
			CMPSC_ASSERT0(0, "CRUD_IO_UNIT_TEST : illegal test command.");
			break;

		}

// This is my own validation step that I wrote. It's almost identical to DEEP_DEBUG
// except for some of the object references and function calls.		
#if DAVE_DEBUG
		// VALIDATION STEP: Ensure our local is like object store
		CrudRequest request;
		CrudResponse response;
		CrudOID oid;
		CRUD_REQUEST_TYPES req;
		uint32_t length;
		uint8_t res, flags;

		request = crud_create_request_read( currentOID );
		response = crud_bus_request( request, tbuf );
		if( crud_operation_success( response ) == CRUD_FAILURE )
		{
			logMessage( LOG_ERROR_LEVEL, "Read failure!\n" );
			return (-1);
		}

		length = crud_extract_size( response );
		oid = crud_extract_oid( response );
		res = crud_operation_success( response );

		if( (cio_utest_length != length) || (memcmp(cio_utest_buffer, tbuf, length)) )
		{
			logMessage( LOG_ERROR_LEVEL, "I have some bad news, you done goofed.\n" );
			bufToString((unsigned char *)tbuf, length, (unsigned char *)lstr, 1024);
			logMessage(LOG_INFO_LEVEL, "D_UTEST VR: %s\n", lstr );
			bufToString((unsigned char *)cio_utest_buffer, length, (unsigned char *)lstr, 1024);
			logMessage(LOG_INFO_LEVEL, "D_UTEST VU: %s\n", lstr );
			return (-1);
		}

		// Print out the buffer
		bufToString((unsigned char *)cio_utest_buffer, cio_utest_length, (unsigned char *)lstr, 1024 );
		logMessage(LOG_INFO_LEVEL, "CIO_UTEST: %s", lstr);
#endif

#if DEEP_DEBUG
		// VALIDATION STEP: ENSURE OUR LOCAL IS LIKE OBJECT STORE
		CrudRequest request;
		CrudResponse response;
		CrudOID oid;
		CRUD_REQUEST_TYPES req;
		uint32_t length;
		uint8_t res, flags;

		// Make a fake request to get file handle, then check it
		request = construct_crud_request(file_table[0].object_handle, CRUD_READ, CRUD_MAX_OBJECT_SIZE, 0, 0);
		response = crud_bus_request(request, tbuf);
		if ((deconstruct_crud_request(response, &oid, &req, &length, &flags, &res) != 0) || (res != 0))  {
			logMessage(LOG_ERROR_LEVEL, "Read failure, bad CRUD response [%x]", response);
			return(-1);
		}
		if ( (cio_utest_length != length) || (memcmp(cio_utest_buffer, tbuf, length)) ) {
			logMessage(LOG_ERROR_LEVEL, "Buffer/Object cross validation failed [%x]", response);
			bufToString((unsigned char *)tbuf, length, (unsigned char *)lstr, 1024 );
			logMessage(LOG_INFO_LEVEL, "CIO_UTEST VR: %s", lstr);
			bufToString((unsigned char *)cio_utest_buffer, length, (unsigned char *)lstr, 1024 );
			logMessage(LOG_INFO_LEVEL, "CIO_UTEST VU: %s", lstr);
			return(-1);
		}

		// Print out the buffer
		bufToString((unsigned char *)cio_utest_buffer, cio_utest_length, (unsigned char *)lstr, 1024 );
		logMessage(LOG_INFO_LEVEL, "CIO_UTEST: %s", lstr);
#endif

	}

	// Close the files and cleanup buffers, assert on failure
	if (crud_close(fh)) {
		logMessage(LOG_ERROR_LEVEL, "CRUD_IO_UNIT_TEST : Failure read comparison block.", fh);
		return(-1);
	}
	free(cio_utest_buffer);
	free(tbuf);

	// Return successfully
	return(0);
}